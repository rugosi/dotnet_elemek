﻿using System;
using System.IO;
using System.Collections.Generic;


namespace tanultak
{
    class Program
    {
        struct Szemely
        {
          public string vezeteknev;
          public string keresztnev;
          public int szuletett;
          public int meghalt;
        }

        static string[] Feldarabol(string x, char szeparator)
        {
          int darab = 0;
          int pozicio = 0;
          string[] tomb = new string[4];
          while(pozicio < x.Length && darab < 4)
          {
            string uj = "";
            while(pozicio < x.Length && x[pozicio] != szeparator)
            {
              uj += x[pozicio];
              pozicio++;
            }
            pozicio++;
            tomb[darab] = uj;
            darab++;
          }
          return tomb;  
        }

        static Szemely[] Atmeretez(Szemely[] eredeti, int ujmeret)
        {
          Szemely[] ujtomb = new Szemely[ujmeret];
          for(int i = 0; i < eredeti.Length; i++)
          {
            ujtomb[i] = eredeti[i];
          }
          return ujtomb;
        }

        static List<Szemely> BeolvasListaba(string fajlnev)
        {
          List<Szemely> L = new List<Szemely>();
          StreamReader s = new StreamReader(fajlnev);
          while(! s.EndOfStream)
          {
            string adat = s.ReadLine();

          /* alternatíva: saját függvény a beépített helyett */
            //string[] adatok = adat.Split(" ");
            string[] adatok = Feldarabol(adat, ' ');

            if (adatok.Length != 4) {
              continue;
            }
            Szemely sz;
            sz.vezeteknev = adatok[0];
            sz.keresztnev = adatok[1];
            sz.szuletett = Convert.ToInt32(adatok[2]);
            sz.meghalt = Convert.ToInt32(adatok[3]);
            L.Add(sz);
          }
          return L;
        }


        static Szemely[] BeolvasTombbe(string fajlnev)
        {
          Szemely[] T = new Szemely[1];
          StreamReader s = new StreamReader(fajlnev);
          int darab = 0;
          while(! s.EndOfStream)
          {
            string adat = s.ReadLine();
            string[] adatok = adat.Split(" ");
            if (adatok.Length != 4) {
              continue;
            }
            if (darab >= T.Length)
            {
              Array.Resize(ref T, darab * 2);
              Console.WriteLine($"Átméretezve: {darab} -> {T.Length}");
            }
            
            T[darab].vezeteknev = adatok[0];
            T[darab].keresztnev = adatok[1];
            T[darab].szuletett = Convert.ToInt32(adatok[2]);
            T[darab].meghalt = Convert.ToInt32(adatok[3]);
            darab++;
          }
          /* alternatíva: saját függvény a beépített helyett */
          //Array.Resize(ref T, darab);
          T = Atmeretez(T, darab);
          return T;
        }

        static void SzemelyKiiro(Szemely sz)
        {
          Console.Write($"{sz.vezeteknev,15} {sz.keresztnev, 15}");
          Console.Write($"{sz.szuletett, 8} {sz.meghalt,5}");
          Console.WriteLine($"{sz.meghalt - sz.szuletett, 5}");
        }

        static void TombKiiro(Szemely[] t)
        {
          Console.WriteLine("{0,15} {1, 15} {2,6} {3,5} {4,5}",
              "Vezetéknév", "Keresztnév", "+", "-", "Élt");
          Console.WriteLine("".PadLeft(50, '-'));
          foreach(Szemely x in t)
          {
            SzemelyKiiro(x);
          }
        }

        static void Main(string[] args)
        {
          List<Szemely> szemelylista = new List<Szemely>();
          Szemely[] szemelytomb = new Szemely[10];

          string fajlnev = @"adatok.txt";

          szemelylista = BeolvasListaba(fajlnev);
          Console.WriteLine($"Lista hossza: {szemelylista.Count}");

          szemelytomb = BeolvasTombbe(fajlnev);
          Console.WriteLine($"Tömb hossza: {szemelytomb.Length}");

          TombKiiro(szemelytomb);
          Array.Sort(szemelytomb, (A,B) => (A.meghalt-A.szuletett).CompareTo(B.meghalt-B.szuletett)); 
          TombKiiro(szemelytomb);

          SortedDictionary<int, int> szulevek = new SortedDictionary<int, int>();
          foreach(Szemely x in szemelytomb)
          /* alternatíva: listával tömb helyett */
          //foreach(Szemely x in szemelylista)
          {
            if (! szulevek.ContainsKey(x.szuletett))
            {
              szulevek[x.szuletett] = 1;
            } else {
              szulevek[x.szuletett]++;
            }
          }

          Console.WriteLine("Születések:");
          foreach(int x in szulevek.Keys)
          {
            Console.WriteLine($"{x} = {szulevek[x]}");
            foreach(Szemely sz in szemelytomb)
            {
              if (sz.szuletett == x)
              {
                SzemelyKiiro(sz);
              }
            }
          }

          StreamWriter sw = new StreamWriter(@"kiirandus.txt");

          foreach(Szemely sz in szemelytomb)
          {
            sw.WriteLine($"{sz.keresztnev}");
          }
          sw.Close();

        }
    }
}
